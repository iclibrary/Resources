# On Organization Stalin

### Problems of Organizational Leadership

**From the report of Joseph Stalin to the 17th Congress of the Communist Party of the Soviet Union, 1934.**

Some people think that it is sufficient to draw up a correct Party line, proclaim it from the housetops, state it in the form of general theses and resolutions, and take a vote and carry unanimously for victory to come of itself, spontaneously, as it were. This, of course, is wrong. It is a gross delusion. Only incorrigible bureaucrats and red-tapists can think so. As a matter of fact, these successes and victories did not come spontaneously, but as the result of a fierce struggle for the application of the Party line. Victory never comes by itself -- it usually has to be attained. Good resolutions and declarations in favour of the general line of the Party are only a beginning; they merely express the desire for victory, but not the victory itself. After the correct line has been laid down, after a correct solution of the problem has been found, success depends on how the work is organized; on the organization of the struggle for the application of the Party line; on the proper selection of personnel; on the way a check is kept on the fulfillment of the decisions of the leading bodies. Otherwise the correct line of the Party and the correct solutions are in danger of being seriously prejudiced.

Furthermore, after the correct political line has been laid down, organizational work decides everything, including the fate of the political line itself, its success or failure. As a matter of fact, victory was achieved and won by a stern and systematic struggle against all sorts of difficulties that stood in the way of carrying out the Party line; by overcoming the difficulties; by mobilizing the Party and the working-class for the purpose of overcoming the difficulties; by organizing the struggle to overcome the difficulties; by removing inefficient executives and choosing better ones, capable of waging the struggle against difficulties.

What are these difficulties; and wherein are they lodged?

They are difficulties attending our organizational work, difficulties
 attending our organizational leadership. They are lodged in ourselves,
 in our leading people, in our organizations, in the apparatus of our 
Party, state, economic, trade union, Young Communist League, and all 
other organizations....

Bureaucracy and red tape in the administrative apparatus; idle chatter about "leadership in general" instead of real and concrete leadership; the functional structure of our organizations and lack of individual responsibility; lack of personal responsibility in work, and wage equalization; the absence of a systematic check upon the 
fulfillment of decisions; fear of self-criticism -- these are the sources of our difficulties; this is where our difficulties are now lodged.

It would be naïve to think that these difficulties can be overcome by means of resolutions and decisions. The bureaucrats have long become past masters in the art of demonstrating their loyalty to Party and government decisions in words, and pigeon-holing them in deed. In order to overcome these difficulties it was necessary to put an end to the disparity between our organizational work and the requirements of the 
political line of the Party; it was necessary to raise the level of organizational leadership in all spheres of the national economy to the level of political leadership; it was necessary to see to it that our organizational work guarantees the practical realization of the political slogans and decisions of the Party.

In order to overcome these difficulties and achieve success it was necessary to *organize* the struggle to eliminate these difficulties; it was necessary to draw the masses of the workers and peasants into this struggle; it was necessary to mobilize the Party itself; it was necessary to purge the Party and the economic organizations of unreliable, unstable and demoralized elements.

What was needed for this?

**We had to organize:**

1. Extensive self-criticism and exposure of the defects in our work;
2. The mobilization of the Party, state, economic, trade union, 
   and Young Communist League organizations for the struggle against 
   difficulties;
3. The mobilization of the masses of the workers and peasants to 
   fight for the application of the slogans and decisions of the Party and 
   of the Government;
4. The extension of emulation and shock work among the working people;
5. A wide network of Political Departments of machine and tractor 
   stations and state farms and the bringing of the Party and Soviet 
   leadership closer to the villages;
6. The division of the People's Commissariats, head offices, trusts, and the
   establishment of closer contact between the business leadership and the enterprises;
7. The elimination of lack of personal responsibility in work and the elimination of wage equalization;
8. The abolition of the "functional" system; the extension of 
   individual responsibility, and a policy directed towards doing away with
   collegium management;
9. The exercise of greater control over the fulfillment of 
   decisions, while taking the line towards reorganizing the Central 
   Control Commission and the Workers' and Peasants' Inspection with a view
   to the further enhancement of the work of checking up on the 
   fulfillment of decisions;
10. The transfer of qualified workers from offices to posts that will bring them into closer contact with production;
11. The exposure and expulsion from the administrative apparatus of incorrigible bureaucrats and red-tapists;
12. The removal from their posts of people who violate the 
    decisions of the Party and the Government, of "window-dressers" and 
    windbags, and promotion to their place of new people -- business-like 
    people, capable of concretely directing the work entrusted to them and 
    of tightening Party and state discipline;
13. The purging of state and economic organizations and the reduction of their staffs;
14. Lastly, the purging of the Party of unreliable and demoralized persons.

These, in the main, are the measures which the Party has had to adopt in order to overcome difficulties, to raise our organizational work to the level of political leadership, and in this way to ensure the application of the Party line.

You know that this is exactly how the Central Committee of the Party carried on its organizational work during the period under review. 

In this, the Central Committee was guided by the brilliant thought uttered by Lenin to the effect that the main thing in organizational work is -- *choosing the right people and keeping a check on the fulfillment of decisions*.

 In regard to choosing the right people and dismissing those who fail to justify the confidence placed in them, I would like to say a few 
words. 

Apart from the incorrigible bureaucrats and red-tapists, as to whose removal there are no differences of opinion among us, there are two other types of executives who retard our work, hinder our work, and hold up our advance. 

One of these types of executives is represented by people who rendered certain services in the past, people who have become aristocrats, who consider that Party decisions and the laws issued by the Soviet Government are not written for them, but for fools. These are the people who do not consider it their duty to fulfill the decisions of the Party and of the Government, and who thus destroy the foundations of Party and state discipline. What do they count upon when they violate Party and Soviet laws? They Presume that the Soviet Government will not have the courage to touch them, because of their past services. These over-conceited aristocrats think that they are irreplaceable, and that they can violate the decisions of the leading bodies with impunity. What is to be done with executives of this kind? They must unhesitatingly be removed from their leading posts, irrespective of past services. *(Voices: "Hear, hear!")* They must be demoted to lower positions, and this must be announced in the Press. *(Voices: "Hear, hear!")* This must be done in order to knock the pride out of these over-conceited aristocrat-bureaucrats, and to put them in their proper place. This must be done in order to tighten up Party and Soviet discipline in the whole of our work. *(Voices: "Hear, hear!" Applause.)* 

And now about the second type of executives. I have in mind the windbags. I would say, honest windbags *(laughter)*,
 people who are honest and loyal to the Soviet Government, but who are 
incompetent as executives, incapable of organizing anything. Last year I had a conversation with one such comrade, a very respected comrade, but an incorrigible windbag, capable of drowning any living cause in a flood of talk. Here is the 
conversation.

> **I:** How are you getting on with the sowing?  
> **He:** With the sowing, Comrade Stalin? We have mobilized ourselves. *(Laughter.)*  
> **I:** Well, and what then?  
> **He:** We have put the question squarely. *(Laughter.)*  
> **I:** And what next?  
> **He:** There is a turn, Comrade Stalin; soon there will be a turn. *(Laughter.)*  
> **I:** But still?  
> **He:** We can say that there is an indication of some progress. *(Laughter.)*  
> **I:** But for all that, how are you getting on with the sowing?  
> **He:** So far, Comrade Stalin, we have not made any headway with the sowing. *(General Laughter.)*

Here you have the physiognomy of the windbag. They have mobilized 
themselves, they have put the question squarely, they have made a turn 
and some progress, but things remain as they were.

This is exactly how a Ukrainian worker recently described the state 
of a certain organization when he was asked whether that organization 
had any definite line: "Well," he said, "they have a line all right, but
 they don't seem to be doing any work." *(General Laughter.)* Evidently that organization also has its quota of honest windbags.

And when such windbags are dismissed from their posts and are given 
jobs far removed from operative work, they shrug their shoulders in perplexity and ask : "Why have we been dismissed? Did we not do all that was necessary to get the work done? Did we not organize a rally of shock workers? Did we not proclaim the slogans of the Party and of the government at the conference of shock workers? Did we not elect the whole of the Political Bureau of the Central Committee to the Honorary Presidium? *(General Laughter.)* Did we not send greetings to Comrade Stalin -- what more do they want of us?" *(Loud Laughter.)*

What is to be done with these incorrigible windbags? Why, if they were allowed to remain on operative work they would drown every living cause in a flood of watery and endless speeches. Obviously, they must be removed from leading posts and given work other than operative work. There is no place for windbags on operative work. *(Voices: "Hear, hear!" Applause.)*

I have already briefly reported on how the Central Committee handled the selection of personnel for the Soviet and economic organizations, and how it pursued the work of keeping a closer check on the fulfillment of decisions. Comrade Kaganovitch will deal with this in greater detail in his report on the third item of the agenda of the Congress.

I would like to say a few words, however, about future work in 
connection with the task of keeping a closer check on the fulfillment of
 decisions.

The proper organization of the work of checking up on the fulfillment of decisions is of decisive importance in the fight against bureaucracy and office routine. Are the decisions of the leading bodies carried out, or are they pigeonholed by bureaucrats and red-tapists? Are they carried out properly, or are they distorted? Is the apparatus working conscientiously and in a Bolshevik manner, or is it running with the 
clutch out? These things can be promptly found out only if a proper check is kept on the fulfillment of decisions. A proper check on the fulfillment of decisions is a searchlight which helps to reveal how the apparatus is functioning at any moment, exposing bureaucrats and red-tapist to full view. We can say with certainty that nine-tenths of our defects and failures are due to the lack of a properly organized 
system of check-up on the fulfillment of decisions. There can be no doubt that had there been such a system of check-up on fulfillment defects and failures would certainly have been averted.

But for the work of checking up on fulfillment to achieve its purpose, two conditions at least are required: first, that fulfillment be checked up systematically and not spasmodically; second, that the work of checking up on fulfillment in all the links of the Party, state, and economic organizations be entrusted not to second-rate people, but 
to people with sufficient authority, the leaders of the organizations concerned....

Our tasks in the sphere of organizational work are:

1. To continue to adapt our organizational work to the requirements of the political line of the party;
2. To raise organizational leadership to the level of political leadership;
3. To see to it that organizational leadership is fully equal to 
   the task of ensuring the realization of the political slogans and 
   decisions of the party.

# Cadres Decide Everything

#### Iosif Stalin, Address Delivered in the Kremlin Palace to the Graduates of the Red Army Academies. May 4, 1935

Comrades, it cannot be denied that in the last few years we have achieved great successes both in the sphere of construction and in the sphere of administration. In this connection there is too much talk about the services rendered by chiefs, by leaders. They are credited with all, or nearly all, of our achievements. That, of course, is wrong,it is incorrect. It is not merely a matter of leaders. But it is not of this I wanted to speak today. I should like to say a few words about cadres, about our cadres in general and about the cadres of our Red Army in particular.

You know that we inherited from the past a technically backward, impoverished and ruined country. Ruined by four years of imperialist war, and ruined again by three years of civil war, a country with a semi-illiterate population, with a low technical level, with isolated industrial oases lost in a sea of dwarf peasant farms — such was the 
country we inherited from the past. The task was to transfer this country from mediaeval darkness to modern industry and mechanized agriculture. A serious and difficult task, as you see. The question that confronted us was: Either we solve this problem in the shortest possible time and consolidate socialism in our country, or we do not solve it, in which case our country — weak technically and unenlightened in the cultural sense — will lose its independence and become a stake in the game of the imperialist powers.

At that time our country was passing through a period of an appalling dearth of technique. There were not enough machines for industry. There were no machines for agriculture. There were no machines for transport. There was not that elementary technical base without which the reorganization of a country on industrial lines is inconceivable. There were only a few of the necessary prerequisites for the creation of such a base. A first-class industry had to be built up. This industry had to 
be so directed as to be capable of technically reorganizing not only industry, but also agriculture and our railway transport. And to achieve this it was necessary to make sacrifices and to exercise the most rigorous economy in everything; it was necessary to economize on food, on schools, on textiles, in order to accumulate the funds required for building up industry. There was no other way of overcoming the dearth of technique. That is what Lenin taught us, and in this matter we followed
in the footsteps of Lenin.

Naturally, uniform and rapid successes could not be expected in so great and difficult a task. In a task like this successes become apparent only after several years. We therefore had to arm ourselves with strong nerves, Bolshevik grit, and stubborn patience to overcome our first failures and to march unswervingly towards the great goal, permitting no wavering or uncertainty in our ranks.

You know that that is precisely how we set about this task. But not all our comrades had the necessary spirit, patience and grit. There turned out to be people among our comrades who at the first difficulties began to call for a retreat. “Let bygones be bygones,” it is said. That, of course, is true. But man is endowed with memory, and in summing up the results of our work one involuntarily recalls the past. 
(Animation.) Well, then, there were comrades among us who were frightened by the difficulties and began to call on the Party to retreat. They said: “What is the good of your industrialization and collectivization, your machines, your iron and steel industry, tractors, harvester combines, automobiles? You should rather have given us more 
textiles, bought more raw materials for the production of consumer goods, and given the population more of the small things that make life pleasant. The creation of an industry, and a first-class industry at that, when we are so backward, is a dangerous dream.”

Of course, we could have used the 3,000 million rubles in foreign currency obtained as a result of a most rigorous economy, and spent on building up our industry, for importing raw materials and for increasing the output of articles of general consumption. That is also a “plan,”  in a way. But with such a “plan” we would not now have a metallurgical industry, or a machine-building industry, or tractors and automobiles, or aeroplanes and tanks. We would have found ourselves unarmed in face of foreign foes. We would have undermined the foundations of socialism 
in our country. We would have fallen captive to the bourgeoisie, domestic and foreign.

It is obvious that a choice had to be made between two plans: between the plan of retreat, which would have led, and was bound to lead, to the defeat of socialism, and the plan of advance, which led and, as you know, has already brought us to the victory of socialism in our country. We chose the plan of advance, and moved forward along the Leninist road, brushing aside those comrades as people who could see more or less what was under tbeir noses, but who closed their eyes to the immediate 
future of our country, to the future of socialism in our country.

But these comrades did not always confine themselves to criticism and passive resistance. They threatened to raise a revolt in the Party against the Central Committee. More, they threatened some of us with bullets. Evidently, they reckoned on frightening us and compelling us to turn from the Leninist road. These people, apparently, forgot that wc Bolsheviks are people of a special cut. They forgot that neither difficulties nor threats can frighten Bolsheviks. They forgot that we 
had been trained and steeled by the great Lenin, our leader, our teacher, our father, who knew and tolerated no fear in the fight. They forgot that the more the enemies rage and the more hysterical the foes within the Party become, the more ardent the Bolsheviks become for fresh struggles and the more vigorously they push forward.

Of course, it never even occurred to us to turn from the Leninist road. Moreover, once we stood firmly on this road, we pushed forward still more vigorously, brushing every obstacle from our path. True, in pursuing this course we were obliged to handle some of these comrades roughly. But that cannot be helped. I must confess that I too had a hand in it. (Loud cheers and applause.)

Yes, comrades, we proceeded confidently and vigorously along the road of industrializing and collectivizing our coun- try. And now we may consider that the road has been traversed. Everybody now admits that we have achieved tremendous successes along this road. Everybody now admits that we already have a powerful, 
first-class industry, a powerful mechanized agriculture, a growing and improving transport system, an organized and excellently equipped Red Army.

This means that we have in the main emerged from the period of dearth of technique.

But, having emerged from the period of dearth of technique. we have entered a new period, a period, I would say, of a dearth of people, of cadres, of workers capable of harnessing technique, and advancing it. The point is that we have factories, mills, collective farms, state farms, a transport system, an army; we have technique for all this; but we lack people with sufficient experience to squeeze out of this technique all that can be squeezed out of it. Formerly, we used to say that “technique decides everything.” That slogan helped us to put an end to the dearth of technique and to create a vast technical base in every branch of activity for the equipment of our people with first-class technique. That is very good. But it is not enough, it is not enough by 
far. In order to set technique going and to utilize it to the full, we need people who have mastered technique, we need cadres capable of mastering and utilizing this technique according to all the rules of the art. Without people who have mastered technique, technique is dead. In the charge of people who have mastered technique, technique can and should perform miracles. If in our first-class mills and factories, in 
our state farms and collective farms, in our transport system and in our Red Army we had sufficient cadres capable of harnessing this technique, our country would secure results three and four times as great as at present. That is why emphasis must now be laid on people, on cadres, on workers who have mastered technique. That is why the old slogan, “Technique decides everything,” which is a reflection of a period 
already passed, a period in which we suffered from a dearth of technique, must now be replaced by a new slogan, the slogan “Cadres decide everything.” That is the main thing now.

Can it be said that our people have fully grasped and realized the great significance of this new slogan? I would not say so. Otherwise, there would not have been the outrageous attitude towards people, towards cadres, towards workers, which we not infrequently observe in practice. The slogan “Cadres decide everything” demands that our leaders should display the most solicitous attitude towards our workers, “little” and “big,” no matter in what sphere they are engaged, training them assiduously, assisting them when they need support, encouraging them when they show their first successes, promoting them, and so forth. Yet in practice we meet in a number of cases with a soulless, bureaucratic, and positively outrageous attitude towards workers. This, 
indeed, explains why instead of being studied, and placed at their posts only after being studied, people are frequently flung about like pawns. People have learnt to value machinery and to make reports on how many machines we have in our mills and factories. But I do not know of a single instance when a report was made with equal zest on the number of people we have trained in a given period, on how we have assisted people to grow and become tempered in their work. How is this to be explained? It is to be explained by the fact that we have not yet learnt to value 
people, to value workers, to value cadres. I recall an incident in Siberia, where I lived at one time in exile. It was in the spring, at the time of the spring floods.

About 30 men went to the river to pull out timber which had been carried away by the vast, swollen river. Towards evening they returned to the village, but with one comrade missing. When asked where the thirtieth man was, they replied indifferently that the thirtieth man had “remained there.” To my question, “How do you mean, remained there?” they replied with the same indifference, “Why ask — drowned, of course.” And thereupon one of them began to hurry away, saying, “I’ve got to go and water the mare.” When I reproached them with having more concern for animals than for men, one of them said, amid the general approval of the rest: “Why should we be concerned about men? We can always make men. But a mare. . . Just try and make a mare.” (Animation.) Here you have a case, not very significant, perhaps, but very characteristic. It seems to me that the indifference of certain of our leaders to people, to 
cadres, their inability to value people, is a survival of that strange attitude of man to man displayed in the episode in far-off Siberia that I have just related.

And so, comrades, if we want successfully to get over the dearth of people and to provide our country with sufficient cadres capable of advancing technique and setting it going, we must first of all learn to value people, to value cadres, to value every worker capable of benefiting our common cause. It is time to realize that of all the 
valuable capital the world possesses, the most valuable and most decisive is people, cadres. It must be realized that under our present conditions “cadres decide everything.” If we have good and numerous cadres in industry, agriculture, transport, and the army — our country will be invincible. If we do not have such cadres — we shall be lame on both legs.

In concluding my speech, permit me to offer a toast to the health and success of our graduates from the Red Army Academies. It wish them success in the work of organizing and directing the defence of our 
country.

Comrades, you have graduated from institutions of higher learning, in which you received your first tempering. But school is only a preparatory stage. Cadres receive their real tempering in practical 
work, outside school, in fighting difficulties, in overcoming 
difficulties. Remember, comrades, that only those cadres are any good 
who do not fear difficulties who do not hide from difficulties, but who,
 on the contrary, go out to meet difficulties, in order to overcome them
 and eliminate them. It is only in the fight against difficulties that 
real cadres are forged. And if our army possesses genuinely steeled 
cadres in sufficient numbers, it will be invincible.

Your health, comrades! (Stormy applause. All rise. Loud cheers for Comrade Stalin.)

**Source:** I. V. Stalin, Problems of Leninism (Moscow: Foreign Language Publishers, 1934), pp. 767-74

## SELECTION, PROMOTION AND ALLOCATION OF CADRES

#### From J. V. Stalin, ***Problems of Leninism***,

    Regulating the composition of the Party and bringing the leading 
bodies closer to the concrete work of the lower bodies was not, and 
could not be, the only means of further strengthening the Party and its 
leadership. Another means adopted in the period under review was a 
radical improvement in the training of cadres, in the work of selecting,
 promoting and allocating cadres and of testing them in the process of 
work.

   
 The Party cadres constitute the commanding staff of the Party; and 
since our Party is in power, they also constitute the commanding staff 
of the leading organs of state. After a correct political line has been 
worked out and tested in practice, the Party cadres become the decisive 
force in the leadership exercised by the Party and the state. A correct 
political line is, of course, the primary and most important thing. But 
that in itself is not enough. A correct political line is not needed as a
 declaration, but as something to be carried into effect. But in order 
to carry a correct political line into effect, we must

have cadres, people who understand the political line of the Party, who 
accept it as their own line, who are prepared to carry it into effect, 
who are able to put it into practice and are capable of answering for 
it, defending it and fighting for it. Failing this, a correct political 
line runs the risk of being purely nominal.

   
 And here arises the problem of properly selecting cadres and 
fostering them, of promoting new people, of correctly allocating cadres,
 and testing them by work accomplished.

   
 What is meant by properly selecting cadres?

   
 Properly selecting cadres does not mean just gathering around one a 
lot of deps and subs, setting up an office and issuing order after 
order. (*Laughter.*) Nor does it mean abusing one's powers, 
switching scores and hundreds of people back and forth from one job to 
another without rhyme or reason and conducting endless 
"reorganizations." (*Laughter.*)

   
 Proper selection of cadres means:

   
 Firstly, valuing cadres as the gold reserve of the Party and the state, treasuring them, respecting them.

   
 Secondly, knowing cadres, carefully studying their individual merits 
and shortcomings, knowing in what post the capacities of a given worker 
are most likely to develop.

   
 Thirdly, carefully fostering cadres, helping every promising worker 
to advance, not grudging time on patiently "bothering" with such workers
 and accelerating their development.

   
 Fourthly, boldly promoting new and young cadres in time, so as not to
 allow them to stagnate in their old posts and grow stale.

   
 Fifthly, allocating workers to posts in such a way that each feels he
 is in the right place, that each may contribute to our common cause the
 maximum his personal capacities enable him to contribute, and that the 
general trend of the work of

allocating cadres may fully answer to the demands of the political line 
for the carrying out of which this allocation of cadres is designed.

   
Particularly important in this respect is the bold and timely promotion of new and young cadres. It seems to me that our people are not quite clear on this point yet. Some think that in selecting people we must chiefly rely on the old cadres. Others, on the contrary, think that we must chiefly rely on young cadres. It seems to me that both are mistaken. The old cadres, of course, are a valuable asset to the Party 
and the state. They possess what the young cadres lack, namely, tremendous experience in leadership, a steeled knowledge of Marxist-Leninist principles, knowledge of affairs, and a capacity for orientation. But, firstly, there are never enough old cadres, there are far less than required, and they are already partly going out of 
commission owing to the operation of the laws of nature. Secondly, part of the old cadres are sometimes inclined to keep a too persistent eye on the past, to cling to the past, to stay in the old rut and fail to observe the new in life. This is called losing the sense of the new. It is a very serious and dangerous shortcoming. As to the young cadres, they, of course, have not the experience, the schooling, the knowledge 
of affairs and the capacity of orientation of the old cadres. But, firstly, the young cadres constitute the vast majority; secondly, they are young, and as yet are not subject to the danger of going out of commission; thirdly, they possess in abundance the sense of the new, which is a valuable quality in every Bolshevik worker; and, fourthly, they develop and acquire knowledge so rapidly, they press upward so eagerly, that the time is not far off when they will overtake the old

fellows, take their stand side by side with them, and become worthy of replacing them. Consequently, the point is not whether we should rely on the old cadres or the new, but rather that we should steer for a combination, a union of the old and the young cadres in one common symphony of leadership of the Party and the state. (*Prolonged applause.*)

   
 That is why we must boldly and in good time promote young cadres to leading posts.

   
One of the important achievements of the Party during the period under review in the matter of strengthening the Party leadership is that, when selecting cadres, it has successfully pursued, from top to bottom, just this course of combining old and young workers.

   
Data in the possession of the Central Committee of the Party show that during the period under review the Party succeeded in promoting to leading state and Party posts over 500,000 young Bolsheviks, members of the Party and people standing close to the Party, over 20 per cent of whom were women.

   
 What is our task now?

   
Our task now is to concentrate the work of selecting cadres, from top to bottom, in the hands of one body and to raise it to a proper, scientific, Bolshevik level.

   
This entails putting an end to the division of the work of studying, promoting and selecting cadres among various departments and sectors and concentrating it in one body.

   
This body should be the Cadres Administration of the Central Committee of the C.P.S.U.(B.) and a corresponding cadres department in each of the republican, territorial and regional Party organizations.

##### 

## CADRES by Georgi Dimitrov from Unity of the Working Class against Fascism

##### 

Comrades, our best resolutions will remain scraps of paper if we lack the people who can put them into effect. Unfortunately, however, I must state that the problem of *cadres,* one of the most important questions facing us, has received almost no attention at this Congress.

The report of the Executive Committee of the Communist International was discussed for seven days, there were many speakers from various countries, but only a few, and they only in passing, discussed this question, so extremely vital for the Communist Parties and the labour movement, In their practical work our Parties have not yet realize by far that *people, cadres, decide everything*.

A negligent attitude to the problem of cadres is all the more impermissible as we are constantly losing some of the most valuable of our cadres in the struggle. For we are not a learned society but a militant movement which is constantly in the firing line. Our most energetic, most courageous and most class-conscious elements are in the 
front ranks. It is precisely these front-line men that the enemy hunts down, murders, throws into jail and concentration camps and subjects to excruciating torture, particularly in fascist countries. This gives rise to the urgent necessity of constantly replenishing the ranks, cultivating and training new cadres as well as carefully preserving the existing cadres.

The problem of cadres is of particular urgency for the additional reason that under our influence the mass united front movement is gaining momentum and bringing forward many thousands of new working-class militants. Moreover, it is not only voting revolutionary elements, not only workers just becoming revolutionary, who have never 
before participated in a political movement, that stream into our ranks. Very often former members and militants of the Social Democratic Parties also join us. These new cadres require special attention, particularly in the illegal Communist Parties, the more so because in their practical work these cadres with their poor theoretical training 
frequently come up against very serious political problems which they have to solve for themselves. The problem of what should be the correct policy with regard to 
cadres is a very serious one for our Parties, as well as for the Young Communist League and for all other mass organizations - for the entire revolutionary labour movement.

What does a correct policy. with regard to cadres imply?

First, *knowing one's people*. As a rule there is no systematic study. of cadres in our Parties. Only, recently have the Communist Parties of France and Poland and, in the East, the Communist Party of China, achieved certain successes in this direction. The 
Communist Party of Germany, before its underground period, had also undertaken a of its cadres. The experience of these Parties has shown that as soon as they began to study their people, Party workers were discovered who had remained unnoticed before. On the other hand, the Parties began to be purged of alien elements who were ideologically and politically harmful. It is sufficient to point to the example of Célor 
and Barbé in France who, when put under the Bolshevik microscope, turned out to be agents of the class enemy, and were thrown out of the Party'. In Hungary the verification of cadres made it easier to discover nests of provocateurs, agents of the enemy, who had sedulously, concealed their identity.

Second, *proper promotion of cadres*. Promotion should not be something casual but one of the normal functions of the Party. It is bad when promotion is made exclusively on the basis of narrow Party considerations, without regard to whether the Communist promoted has contact with the masses or not. Promotion should take place on the basis of the ability, of the various Party workers to discharge particular 
functions, and of their popularity, among the masses. We have examples in our Parties of promotions which have produced excellent results. For instance, we have a Spanish woman Communist, sitting in the Presidium of this Congress, Comrade Dolores. Two years ago she was still a rank-and-file Party-worker. But in the very first clashes with the class enemy she proved to be an excellent agitator and fighter. Subsequently.
promoted to the leading body. of the Party, she has proved herself a most worthy member of that body.

I could point to a number of similar cases in several other countries, but in the majority of cases promotions are made in an unorganized and haphazard manner, and therefore are not always fortunate. Sometimes moralizers, phrasemongers and chatterboxes who actually harm the cause are promoted to leading positions.

Third, *the ability to use people to the best advantage*. We must be able to ascertain and utilize the valuable qualities of every, single active member. There are no ideal people; we must take them as they are and correct their weaknesses and shortcomings. We know of glaring examples in our Parties of the wrong utilization of good, honest
Communists who might have been very useful had they, been given work that they were better fit to do.

Fourth, *proper distribution of cadres*. First of all, we must see to it that the main links of the movement are in the hands of capable people who have contacts with the masses, who have sprung from the grassroots, who have initiative and are staunch. The more important districts should have an appropriate number of such activists. In 
capitalist countries it is not an easy matter to transfer cadres from one place to another. Such a task encounters a number of obstacles and difficulties, including lack of funds, family considerations, etc., difficulties which must be taken into account and properly overcome. But usually we neglect to do this altogether.

Fifth, *systematic assistance to cadres*. This assistance should consist in detailed instruction, in friendly check-up, in correction of shortcomings and errors, and in concrete day-to-day guidance of cadres.

Sixth, *care for the preservation of cadres*. We must learn promptly to withdraw Party workers to the rear whenever circumstances so require and replace them by others. We must demand that the Party, leadership, particularly in countries where the Parties are illegal, assume paramount resposibility for the preservation of cadres. The 
proper preservation of cadres also presupposes a highly efficient organization of secrecy in the Party. In some of our Parties many, comrades think that the Parties are already prepared for the event of illegality even though they, have reorganized them only formally, according to ready-made rules. We had to pay very dearly for having 
started the real work of reorganization only after the Party had gone underground under the direct heavy blows of the enemy. Remember the severe losses the Communist Party of Germany suffered during its transition to underground conditions. Its experience should serve as a serious warning to those of our Parties which today are still legal but may lose their legal status tomorrow.

Only, a correct policy in regard to cadres will enable our Parties to develop and utilize all available forces to the utmost, and obtain from the enormous reservoir of the mass movement ever fresh reinforcements of new and] better active workers.

What should be our main criterion in selecting cadres?

First, *absolute devotion* to the cause of the working class, *loyalty to the Party,* tested in face of the class enemy - in battle, in prison, in court. Second, *the closest possible contact with the masses.* The comrades concerned must be wholly absorbed in the interests of the masses, feel the life pulse of the masses, know their sentiments and 
requirements. The prestige of the leaders of our Party organizations should be based, first of all, on the fact that the masses regard them as their leaders and are convinced through their own experience of their ability as leaders and of their determination and self-sacrifice in struggle.

Third, ability *independently to find one's bearings* in given circumstances and not to be afraid of *assuming responsibility in making decisions.* He who fears to take responsibility is not a leader. He who is unable to display initiative, who says: 'I will do only what I am told,' is not a Bolshevik. Only he is a real Bolshevik leader who does not lose his head
at moments of defeat, who does not get a swelled head at moments of success, who displays indomitable firmness in carrying out decisions. Cadres develop and grow best when they are placed in the position of having to solve concrete problems of the struggle independently, and are aware that they are fully responsible for their decisions.

Fourth, *discipline* and *Bolshevik hardening in* the struggle against the class enemy as well as in their irreconcilable opposition to all deviations from the Bolshevik line.

We must place all the more emphazis on these conditions which determine the correct selection of cadres, because in practice preference is very often given to a comrade who, for example, is able to write well and is a good speaker, but is not a man or woman of action, and is not suited for the struggle as some other comrade who may not be able to write or speak so well, but is staunch comrade, possessing initiative and contact with the masses, and is capable of going into battle and leading others into battle. Have there not been many cases of sectarians, doctrinaires or moralizers crowding out loyal mass workers, genuine workingclass leaders?

Our leading cadres should combine the knowledge of what they must do with *Bolshevik stamina revolutionary strength of character* and *the power to carry it through*.

In connection with the question of cadres, permit me, comrades, to dwell also on the very great part which the International Labour Defence is called upon to play, in relation to the cadres of the labour movement. The material and moral assistance which the ILD organizations render to our prisoners and their families, to political emigrants, to persecuted revolutionaries and anti-fascists, has saved the lives and 
preserved the strength and fighting capacity of thousands upon thousands of most valuable fighters of the working class in many countries. Those of us who have been in jail have found out directly, through our own experience the enormous significance of the activity of the ILD.

By, its activity the ILD has won the affection, devotion and deep gratitude of hundreds of thousands of proletarians and of revolutionary elements among the peasantry and intellectuals.

Under present conditions, when bourgeois reaction is growing, when fascism is raging and the class struggle is becoming more acute, the role of the ILD is increasing immensely. The task now before the ILD is to become a genuine mass organization of the working people in all capitalist Countries (particularly, in fascist countries, where it must adapt itself to the special conditions prevailing there). It must become, so to speak, a sort of 'Red Cross' of the united front of the proletariat and of the anti-fascist Popular Front, embracing millions of working people - the 'Red Cross' of the army of the toiling classes embattled I fascism, fighting for peace and socialism. If the ILD is to 
perform its part successfully,, it must train thousands of its own active militants, a multitude of its own cadres, ILD cadres, answering in their character and capacity to *the special purposes* of this extremely important organization.

And here I must say as categorically, and as sharply as possible that while a *bureaucratic approach* and a soulless attitude towards people is harmful in the labour movement taken in general, in the sphere of activity, of the ILD such an attitude is an evil bordering on *the criminal.* The fighters of the working class, the victims of reaction and fascism who are suffering agony, in torture chambers and concentration camps, political emigrants and their families, should all meet with the most sympathetic care and solicitude on the part of the organizations and functionaries of the 
ILD. The ILD must still better appreciate and discharge its duty of assisting the fighters in the proletarian and anti-fascist movement, particularly in physically and morally preserving the cadres of the workers' movement. The Communists and revolutionary workers who are active in the ILD organizations must realize at every step the enormous responsibility they have before the working class and the Communist 
International for the successful fulfilment of the role and tasks of the ILD.

Comrades, as you know, cadres receive their best train in the *process of struggle,* in surmounting difficulties and withstanding tests, and also *from favourable and unfavourable examples of conduct.* We have hundreds of exampies of splendid conduct in times of strikes, during demonstrations, in jail, in court. We have thousands of instances of heroism, but unfortunately also not a few cases of faintheartedness,
lack of firmness and even desertion. We often forget these examples, both good and bad. We do not teach people to benefit by these examples. We do not show them *what* should be emulated and *what* rejected. We must study, the conduct of our comrades and militant workers during class conflicts, under police interrogation, in the jails and 
concentration camps, in court, etc. The good examples should be brought to light and held up as models to be followed, and all that is rotten, non-Bolshevik and philistine should be cast aside. Since the Reichstag Fire Trial we have had quite a few comrades whose statements before bourgeois and fascist courts show that numerous cadres are growing up with an excellent understanding of what really constitutes Bolshevik 
conduct in court.

But how many even of you, delegates to the Congress, know the details of the trial of the railwaymen in Rumania, know about the trial of Fiete Schulze, who was subsequently beheaded by the fascists in Germany,the trial of our valiant Japanese comrade Itsikawa, the trial of the Bulgarian revolutionary soldiers, and many other trials at which admirable examples of proletarian heroism were displayed?

Such worthy examples of proletarian heroism must be popularized, must be contrasted with the manifestations of faint-heartedness, philistinism, and every kind of rottenness and frailty in cur ranks and the ranks of the working class. These examples must be used most extensively, in educating the cadres of the workers' movement.

Comrades, our Party leaders often complain that there are no *people,* that they are short of people for agitational and propaganda work, for the newspapers, the trade unions, for work among the youth, among women. Not enough, not enough - that is the cry. We simply haven't got the people. To this we could reply in the old yet eternally new words of Lenin:

There are no people - yet there are enormous numbers of people. *There are enormous numbers of people, because the working class and ever more diverse strata of society, year after year, throw up from their ranks an increasing number of discontented people who desire to protest.... At the same time we have no people, because we have... no talented 
organizers capable of organizing extensive and at the same time uniform and harmonious work that would give employment to all forces, even the most inconsiderable.* (V. I. Lenin, *Collected Works*, Vol. 5, pp. 436-437)

These words of Lenin must be throughly grasped by our Parties and  applied by them as a guide in their everyday work. There are plenty of people. They need only to be discovered in our own organizations, during strikes and demon strations, in various mass organizations of the workers, in united front bodies. They must be helped to grow in the course of their work and struggle, they must be put in a situation where
 they can really be useful to the workers cause.

Comrades, we Communists are people of action. Ours is the problem of practical struggle against the offensive of capital, against fascism and the threat of imperialist war, the struggle for the overthrow of capitalism. It is precisely this *practical* task that obliges Communist cadres to equip themselves with *revolutionary theory,* for theory gives those engaged in practical work the power of orientation, clarity of vision, assurance in work, belief in the triumph of our cause.

But real revolutionary theory is irreconcilably hostile to all emasculated theorizing, all barren play with abstract definitions. *Our theory is not a dogma, but a guide to action;* Lenin used to say. It is *such a theory* that our cadres need, and they need it as badly as they need their daily bread, as they need air or water. Whoever really wishes to rid our work of deadening, cut-and-dried schemes, of pernicious scholasticism, must burn them out with a red-hot iron, both by *practical,* active struggle waged together with and at the head of the masses, and by *untiring effort* to master the mighty, fertile, all powerful teaching of Marx, Engels, Lenin.

In this connection I consider it particularly necessary to draw your attention to the work of our *Party schools.* It is not pedants, moralizers or adepts at quoting that our schools must train. No. It is practical frontrank fighters in the cause of the working class that should graduate from there, people who are front-rank fighters not only because of their boldness and readiness for self-sacrifice, but also because they see further than rank-and-file workers and know better than they the path that leads to the 
emancipation of the working people. All sections of the Communist International must without any dilly-dallying seriously take up the question of the proper organization of Party schools, in order to turn them into *smithies* where these fighting cadres are forged.

The principal task of our Party schools, it seems to me, is to teach the Party and Young Communist League members there how to apply, the Marxist-Leninist method to the concrete situation in particular countries, to definite conditions, not the struggle against an enemy 'in general,' but against a particular, definite enemy. This makes 
necessary a study of not merely the letter of Leninism, but its living revolutionary spirit.

There are two ways of training cadres in our Party schools:

First method: teaching people abstract theory, trying to give them the greatest possible dose of dry learning, coaching them how to write theses and resolutions in a literary style and only incidentally touching upon the problems of the particular 'country, of the particular labour movement, its history and traditions, and the experience of the 
Communist Party in question.

Second method: theoretical training in which mastering the fundamental principles of Marxism-Leninism is based on practical study by the student of the key problems of the struggle of the proletariat in his own country. On returning to his practical work, the student will then be able to find his bearings by himself, and *become an independent practical organizer and leader capable of leading the masses in battle against the class enemy*.

Not all graduates of our Party schools prove to be suit able. There are many phrases, abstractions, a good deal of book knowledge and show of learning. But we need real truly Bolshevik organizers and leaders of the masses. And we need them badly this very day. It does not matter if such students cannot write good theses (though we need that very much, too), but they must know how to organize and lead undaunted by 
difficulties, capable of surmounting them.

Revolutionary theory is the generalized, *summarized experience* of the revolutionary movement. Communists must carefully utilize in their countries not only the experience of the past but also the experience of the present struggle of other detachments of the international workers' movement. However, correct utilization of experience does not by any means denote *mechanical transposition* of readymade forms and methods of struggle from one set of conditions to another, from one 
country to another, as so often happens in our Parties. Bare imitation, simple copying of methods and forms of work, even of the Communist Party of the Soviet Union, in countries where capitalism is still supreme, may with the best of intentions result in harm rather than good, as has so often actually been the case. It is precisely from the experience of the Russian Bolsheviks that we must learn to apply effectually, to the 
specific conditions of life in each country, *the single international line;* in the struggle against capitalism we must learn pitilessly to cast aside, pillory and hold up to general ridicule all *phrase -mongering, use of hackneyed formulas, pedantry and dogmatism*.

It is necessary to learn, Comrades, to learn always, at every, step, in the course of the struggle, at liberty and in jail. To learn and to fight, to fight and to learn.

Comrades, never has any international congress of Communists aroused such keen interest on the part of world public opinion as we witness now in regard to our present Congress. It may be said without fear of exaggeration that there is not a single serious newspaper, not a single political party, not a single more or less serious political or social leader that is not following the course of our Congress with the closest attention.

The eyes of millions of workers, peasants, small townspeople, office workers and intellectuals, of colonial peoples and oppressed nationalities are turned towards Moscow, the great capital of the *first* but *not the last* state of the international proletariat. In this we see a confirmation of the enormous importance and urgency of the questions discussed at the Congress and of its decisions.

The frenzied howling of the fascists of all countries, particularly of rabid German fascism, only confirms us in the belief 'that our decisions have indeed hit the mark.

In the dark night of bourgeois reaction and fascism in which the class enemy is endeavouring to keep the working masses of the capitalist countries, the Communist International, the international Party. of the Bolsheviks, stands out like a beacon, showing all mankind the one way to emancipation from the voke of capitalism, from fascist barbarity and the horrors of imperialist war.

The establishment of unity of action of the working class *is the decisive* stage on that road. Yes, unity. of action by, the organizations of the working class of every trend, the consolidation of its forces in all spheres of its activity and in all sectors of the class struggle.

The working class must achieve *the unify of its trade unions.* In vain do some reformist trade union leaders attempt to frighten the workers with the spectre of a trade union democracy. destroyed by the interference of the Communist Parties in the affairs of the united trade unions, by the existence of Communist factions within the trade unions. To depict us Communists as opponents of trade union democracy. is sheer nonsense. We advocate and consistently uphold the right of the trade unions to decide their problems for themselves. We are even prepared to forego the creation of Communist factions in the trade unions if that is necessary in the interests of trade union unity. We are prepared to come to an agreement on the independence of the united trade unions from all political parties. But we are decidedly opposed to any *dependence* of the trade unions on the bourgeoise, and do not give up our basic point 
of view that it is impermissible for trade unions to adopt a neutral position in regard to the class struggle between the proletariat and the bourgeoisie.

The working class must strive to secure *the union* of all forces of the working-class youth and of all organizations of the anti-fascist youth, and win over that section of the working youth which has come under the demoralizing influence of fascism and other enemies of the people.

The working class must and will achieve unity of action in all fields of the labour movement. This will come about !he sooner the more firmly and resolutely we Communists and revolutionary workers of all capitalist countries apply. in practice the new tactical line adopted by our Congress in relation to the most important urgent questions of the international workers' movement.

We know that there are many difficulties ahead. Our path is not a smooth asphalt road, our path is not strewn with roses. The working class will have to overcome many an obstacle, including obstacles in its own midst; it faces the task above all of reducing to naught the disruptive machinations of the reactionary elements of Social Democracy.
 Many are the sacrifices that will be exacted under the hammer blows of bourgeois reaction and fascism. The revolutionary ship of the proletariat will have to steer its course through a multitude of submerged rocks before it reaches its port.

But the working class in the capitalist countries is today no longer what it was in 1914, at the beginning of the imperialist war, nor what it was in 1918, at the end of the war. The working class has behind it twenty years of rich experience and revolutionary trials, bitter lessons of a number of defeats, especially in Germany, Austria and Spain.

The working class has before it the inspiring example of the Soviet Union, the land of victorious socialism, an example of how the class enemy can be defeated, how the working class can establish its own government and build a socialist society .

The bourgeoisie no longer holds *undivided* dominion over the whole expanse of the world. Now *the victorious working class* rules over one sixth of the globe. Soviets rule over a vast part of the great China.

The working class possesses a firm, well-knit revolutionary vanguard, the Communist International.

The whole course of historical development, Comrades, favours the cause of the working class. In vain are the efforts of the reactionaries, the fascists of every hue, the entire world bourgeoisie, to turn back the wheel of history. No, that wheel is turning forward and will continue to turn forward towards a worldwide Union of Soviet 
Socialist Republics, until the final victory of socialism throughout the world.

*There is but one thing that the working class of the capitalist countries still lacks - unity in its own ranks*.

So let the battle cry of the Communist International, the clarion call of Marx, Engels and Lenin ring out all the more loudly from this platform to the whole world.

*Workers of all countries, unite...*
