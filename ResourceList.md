# Resources

## 

### Recommended Intros

- Quick lesson on the importance of Political Education from Chairman Fred Hampton **[Fred Hampton on the importance of revolutionary education - Invidious](https://yewtu.be/watch?v=DviCUygm3eM)**
  
  

- [**ABCs of Decolonization by Ena͞emaehkiw Wākecānāpaew Kesīqnaeh**](https://songdogletters.wordpress.com/2018/03/26/abcs-of-decolonization/)

- [**Why Revolutionaries need Marxism** ](https://politicaleducation.org/wp-content/uploads/2017/09/dialego-philosophy-class-struggle.pdf)

- **[Lenin: The Three Sources and Three Component Parts of Marxism](https://www.marxists.org/archive/lenin/works/1913/mar/x01.htm)**

- **[How Lenin Studied Marx](https://www.marxists.org/archive/krupskaya/works/howleninstudiedmarx.htm)**

- **[The Weapon of Theory by Amilcar Cabral](https://www.marxists.org/subject/africa/cabral/1966/weapon-theory.htm)**

- [**When Race Burns Class: Settlers Revisited (An Interview with J. Sakai)**](https://kersplebedeb.com/posts/raceburn/)

- **[Basic Principles of Marxism-Leninism: A Primer,](https://foreignlanguages.press/wp-content/uploads/2020/08/S20-Basic-Principles-of-ML-A-Primer.pdf) [Jose Maria Sison](https://foreignlanguages.press/wp-content/uploads/2020/08/S20-Basic-Principles-of-ML-A-Primer.pdf)** – A comprehensive introduction to Marxism-Leninism, covering the development and theory of dialectical materialism, historical materialism and Marxist-Leninist political economy

- **Decolonial Marxism: Essays from the Pan African Revolution by Walter Rodney** Decolonial Marxism records such a life by collecting previously unbound essays written during the world-turning days of Black revolution. In drawing together pages where he elaborates on the nexus of race and class, offers his reflections on radical pedagogy, outlines programs for newly independent nation-states, considers the challenges of anti-colonial historiography, and produces balance sheets for a dozen wars for national liberation, this volume captures something of the range and power of Rodney’s output. But it also demonstrates the unbending consistency that unites his life and work: the ongoing reinvention of living conception of Marxism, and a respect for the still untapped potential of mass self-rule.

- **[Vita Wa Watu a New Afrikan Theoretical 
  Journal 1988 Book Twelve: 'Notes On Cadre Policy & Development' & 'On What It Means to Re-Build'](https://brothermalcolm.net/TRANSFORMED/PDF/book12.pdf)** **New Afrikans Must Read!**

- **[Instructions to one studying independentLy by  N. K. Krupskaya](https://foreignlanguages.press/wp-content/uploads/2022/05/C35-Krupskaya-On-Education-1st-Printing.pdf)** <mark>Note:</mark> will probably place in section of its own in the repository in conjuction with other education  material.

- **[The Principles of Communism](https://www.marxists.org/archive/marx/works/1847/11/prin-com.htm)**

- **[Settlers: The Mythology of the White Proletariat ](https://readsettlers.org/index.html)**  **Must Read**

---

## Philosophy

#### Dialectics:

- **[ELEMENTARY PRINCIPLES of PHILOSOPHY by Georges Politzer](http://www.readmarxeveryday.org/epop/index.html) Beginner Friendly**

**List from a user from communism101:**

Primary (chronologically):

1. **Marx: Holy Family: The Mystery of Speculative Construction and The Revealed Mystery of The “Standpoint”**. Gives a general critique of the idealist method in the manner of a demystification. Not fully Marxist yet (it's arguably the breaking point with Feuerbach), but important. **[link](https://www.marxists.org/archive/marx/works/1845/holy-family/index.htm)**

2. **Marx: Theses On Feuerbach.** The breakthrough to Marxism, where Marx overcomes both the German Idealist tradition and the old materialism. Very dense and requires a lot of knowledge of the named traditions to fully be grasped, but you can still get something out of it without that knowledge. I'd argue Marx and Engels unfold and develop these theses in German Ideology, which makes things easier. [link](https://www.marxists.org/archive/marx/works/1845/theses/index.htm)

3. **Marx and Engels: German Ideology: Feuerbach: Opposition of the Materialist and Idealist Outlooks**. They develop historical materialism (pay close attention on the stress they put on the relations of production here) and lay down some fundamentals of dialectical materialism. In this interrelated explanation it is clear that both aspects of Marxism cannot be separated without destroying them. [The German Ideology: Chapter 1 - On Feuerbach](https://www.marxists.org/archive/marx/works/1845/german-ideology/ch01.htm)  

4. **Marx: Poverty of Philosophy: The Method.** Proudhon had a second hand, vulgarized understanding of Hegelian dialectics and Marx felt it needed correction and clarification how things really work. In doing that he left us one of his few direct investigations of dialectics.

5. **Marx: Grundrisse: Introduction.** I think this is Marx' deepest, fairly direct treatment of materialist dialectics. Difficult read that should be studied again and again, but there's much of great import here.  

6. **Marx: Contribution: Preface.** Very general lines on historical and dialectical materialism. Very influential. Should be read carefully, because some take a mechanistic and economistic reading out of this that's not Marx' intention.

7. **Marx: Capital I: Prefaces and Afterwords.** Includes general remarks, a longer excerpt from a Russian reviewer on method that Marx approves of and an applied example regarding the decline and transformation of political economy. As Lenin first pointed out, Capital itself is of course the greatest example of the application of the Marxist method.  

8. **Engels: Anti-Dühring. Introductions and Part I: Philosophy.** Engels gives a general outline of Marx' and his views regarding philosophical problems, science and the historical development of human thought. Including three chapters on the most basic movements of materialist dialectics. Later reworked into the Socialism: Utopian and Scientific pamphlet, so I'll only list Anti-Dühring here. Printed versions should also have his notes on this book, which include more interesting and important thoughts on materialist dialectics.  

9. **Engels: Dialectics of Nature.** Engels late, unfinished masterpiece. He studied the natural sciences for more than a decade to write this. Most of it remained fragmentary, however it includes chapters and fragments on dialectics, the Marxist understanding of the sciences and their relation to dialectical thought, a still very important struggle against empiricism that permeates the entire book, etc. In times of the Anthropocene this might be one of the most important Marxist books.  

10. **Engels: Ludwig Feuerbach and the End of Classical German Philosophy.** Engels outlines the development of Marxism out of its heritage in German Idealism. He defends this heritage against the vulgarization of neo-Kantianism that had already set in at that point (see Lukács for the deeper causes of this still ongoing phenomenon of the rot of bourgeois philosophy). Engels was the first to take up the fight against this.  

11. **Stalin: Anarchism Or Socialism?.** One the funniest texts of Marxism, imo. Stalin gives a rundown of the basics of historical and dialectical materialism in an easy to understand, polemical fashion. Great for beginners.  

12. **Lenin: Materialism and Empirio-criticism.**  After the defeat of the 1905-06 Russian Revolution neo-Kantian and positivist philosophical positions took a hold within the ranks of the Bolsheviks, prompting Lenin to write his longest worked out philosophical work. Building on Engels, Feuerbach and Dietzgen, this is largely focused on epistemology, the theory of science and materialism. Contrary to the popular cliché Lenin is not an undialectical thinker here (he never was).  

13. **Lenin: The Three Sources and Three Component Parts of Marxism.** Gives a short theorization of the main elements of Marxism as a whole. 

14. **Lenin: Philosophical Notebooks.** The core of this is Lenin's study of Hegel's Logic, which is in essence a Marxist demystification of Hegel. These studies were crucial in his theorization of imperialism and the struggle against its effects within the labor movement (the revisionism of the Second International, the national question, the labor aristocracy, etc.). Woefully under read, this is Lenin's most important philosophical work and it has influenced the greatest philosophers in Marxism (Mao, Ilyenkov, Lukács).  

15. **Lenin: Karl Marx.** Written during his Hegel studies, this is a masterpiece of theoretical condensation and includes an account of materialist dialectics.

16. **Lenin: Once Again On The Trade Unions.**  Forced by the mistakes of Trotsky and Bukharin, Lenin gives a brief but pointed discussion of the basics of dialectics, with an important differentiation to eclecticism (which Marx already always stressed as characteristic for petite bourgeois thought).  

17. **Lenin: On the Significance of Militant Materialism.**  A brief text in which Lenin stresses the importance of materialist dialectics for the natural sciences in particular as well as the relation of the Marxist philosopher to the natural scientists.  

18. **Mao: On Practice** . A fantastically accessible, deep presentation of the basics of dialectical materialism. **Study Guide** 

19. **Mao: On Contradiction.**  Mao advances the Marxist theory of contradiction in this crucial masterpiece of materialist dialectics. Here as well as in On Practice he built on Lenin's Philosophical Notebooks.  **Study Guide** **Must Read!**

20. **Stalin: Dialectical and Historical Materialism**. Stalin's classical presentation. Much maligned and indeed flawed (arguably its greatest error is a complete omission of the core of dialectics, the unity of opposites), this is still a good introductory text to Marxism. It just shouldn't be read as an exhaustive account of Marxism. **Study Guide**

Suggested reading order for beginners, who want to get an understanding of materialist dialectics and deepen it as they go along (this is certainly up for debate, I'm only speculating on the easiest path, others may disagree): (20), (18), (11), (13), (10), (17), (16), (15), (19), (7), (4), (8), (1), (6), (3), (9), (12), (2), (5), (14).

Secondary (ordered by difficulty, from the most accessible to the most advanced):

1. **Thalheimer: Introduction to Dialectical Materialism.** Easy read well suited for beginners. Has the advantage of giving an historical approach and including interesting stuff on the Indian and Chinese heritage. He's also trying a deduction as the three basic laws of dialectics as given by Engels.  

2. **Plekhanov: The Development of the Monist View of History.** Also accessible (not quite so much as Thalheimer) and with a historical approach (not reaching as deep into time as Thalheimer, but going deeper in terms of content). Very insgihtful regarding the genesis of historical materialism too.  

3. **Ilyenkov: Leninist Dialectics & Metaphysics of Positivism.** A polemic against the positivism taking a hold in the Brezhnev era of the USSR. Ilyenkov defends and contextualizes Lenin's Empiriocriticism polemic and attacks Bogdanov's techno-fetishist visions of the future (relevant for Americans and their still prevalent techno fetish).  

4. **Lukács: What is Orthodox Marxism?.** He's stressing the crucial role of dialectics to Marxism. Written in the struggle against the mechanistic tradition that had developed through the Second International.  

5. **Lukács: Moses Hess and the Problems of Idealist Dialectics.** Brilliant analysis of the dead-ends of the attempts to overcome the Hegelian tradition along the path of idealism. Elucidates the philosophical achievements of Marx and Engels. Still works against these attempts that haven't stopped, naturally (since their roots in bourgeois society persist).  

6. **Pilling: Marx’s Capital. Great elucidation of Marx' method.** Informed by Lenin's Hegel studies, Ilyenkov's study of Capital, Rubin's analysis of commodity fetishism and Rosdolsky's analysis of the Grundrisse. He was a trot, so there's some unnecessary Stalin bashing.  

7. **Ilyenkov: Dialectical Logic.** A book length analysis of the modern dialectical tradition from Descartes to Lenin, critically analyzing the emergence and development of materialist dialectics up to that point. Brilliant but advanced stuff.  

8. **Ilyenkov: Dialectics of the Abstract & the Concrete in Marx’s Capital.** Imo still the finest analysis of Marx' method in Capital.  

9. **Lukács: Destruction of Reason.** Lukács investigates the roots of fascist ideology in the tradition of German philosophy (he's not claiming that this is an exclusively German phenomenon) and the causes for the decline of bourgeois philosophy after Hegel, especially after the bourgeoisie had secured its rule in 1871 and the working class had emerged as the new historical force threatening bourgeois society. Not that difficult to read but necessitates some understanding of Hegel to get the central thesis.
   
   ---

## Historical Materialism

- **The Basic Concepts of Historical Materialism by Marta H**  <mark>NOTE</mark>: **ask for copy being reformated atm**

- **[Historical  Materialism ](https://www.redstarpublishers.org/CornHistMat.pdf)by Maurice Cornforth**

- **[Historical Materialism A System of Sociology](https://www.marxists.org/archive/bukharin/works/1921/histmat/index.htm) by Nikolai Bukharin**

## Marxist Epistomology

- **[Where Do Correct Ideas Come From?, Mao Zedong](https://www.marxists.org/reference/archive/mao/selected-works/volume-9/mswv9_01.htm)** – Short piece from Mao Zedong which summarises the dialectical-materialist theory of knowledge that recognises the primary role of practice and experience in developing understanding.

- See On Practice by Mao above

- **[Theory of Knowledge ](https://www.marxists.org/archive/cornforth/1955/theory-knowledge.pdf)by Maurice CornForth**

## Morality and Ethics

- **[Activist Study/Araling Aktabista](https://foreignlanguages.press/wp-content/uploads/2021/11/S22-Activist-Study-ARAK-4th-Printing.pdf)** – Part of the Filipino revolutionary curriculum that compiles easy-to-understand lessons on revolutionary attitude, revolutionary 
  analysis, the mass line and democratic centralism

- **[ON THE CORRECT HANDLING OF CONTRADICTIONS AMONG THE PEOPLE ](https://www.marxists.org/reference/archive/mao/selected-works/volume-5/mswv5_58.htm)** By Mao

- **[The Correct Handling of a Revolution, In Defense of Self Defense, The Black Panther, July 20, 1967](https://www.marxists.org/archive/newton/1967/07/20.htm) by Huey P. Newton**

- **[On Communist Ethics](https://www.marxists.org/archive/krupskaya/works/ethics.htm) by Nadezhda Krupskaya**

- TBAH:

- Ho Chi Minhs Essay goes here

- [**Ajiths Essay For a  Materialist Ethics pg 66**](https://foreignlanguages.press/wp-content/uploads/2021/11/N09-Of-Concepts-_-Methods-2nd-Printing.pdf)

- Engels on Ethics 

- [**Lenin: Draft and Explanation of a Programme for the Social-Democratic Party**](https://www.marxists.org/archive/lenin/works/1895/misc/x01.htm)

## Other trends in Philosophy

- Siraj. [***Postmodernism Today: A Brief Introduction***](http://www.bannedthought.net/India/PeoplesMarch/PM1999-2006/publications/post-modernism/contents.htm) – An analysis of postmodernist philosophy and ideology from an explicitly Maoist perspective, demonstrating its reactionary relationship to Marxism, and the way it digests concepts of “power” by neutralizing all strategic discussions of *seizing* power. Siraj uses a plethora of quotations from foundational postmodernist texts, and it can be quite rough-going if one is not familiar with them. Nevertheless it is one of the most important texts on postmodernism given its source and purpose: to vindicate the Marxist method in the 21st century. <mark>WARNING</mark> PEOPLE FROM INDIA DO NOT CLICK ITS FROM BANNED THOUGHT

## Philosophy Media

**[Half Nelson Dialectics Classroom Scenes - Invidious](https://yewtu.be/watch?v=H4J2ArKlxnA)**

---

## Political Economy

- The Fundementals of Political Economy **Study Guide Beginner Friendly**

- **Capital I, II, III, Contribution to a critique of political economy**. 

- Lenin's [***Imperialism, the Highest Stage of Capitalism***](https://www.marxists.org/archive/lenin/works/1916/imp-hsc/) – Lenin’s work on imperialism remains one of the most important illuminations of the 20th century, and is pivotal to any coherent analysis of capitalism in the modern era. Although much has been added to the body of knowledge and analysis of imperialism since this work’s 
  publication, it has not (as some would argue) become obsolete as a 
  result, but strengthened and verified by more than a hundred years of 
  experience. This document is at the foundation of modern political 
  economy, and cannot be neglected.

- **Introduction to Neocolonialism by Jack Woodis** **<mark>NOTE</mark>** trying to find a copy

- **Divided World Divided Class: Global Political Economy and the Stratification of Labour Under Capitalism** By Zak Cope
  
  Divided World Divided Class charts the history of the ‘labour aristocracy’ in the capitalist world system, from its roots in colonialism to its birth and eventual maturation into a full-fledged middle class in the age of imperialism. It argues that pervasive national, racial and cultural chauvinism in the core capitalist countries is not primarily attributable to ‘false class consciousness’, ideological indoctrination or ignorance as much left and liberal thinking assumes. Rather, these and related forms of bigotry are concentrated expressions of the major social strata of the core capitalist nations’ shared economic interest in the exploitation and repression of dependent nations.
  The book demonstrates not only how redistribution of income derived from super-exploitation has allowed for the amelioration of class conflict in the wealthy capitalist countries, it also shows that the exorbitant ‘super-wage’ paid to workers there has meant the disappearance of a domestic vehicle for socialism, an exploited working class. Rather, in its place is a deeply conservative metropolitan workforce committed to maintaining, and even extending, its privileged position through imperialism.
  The book is intended as a major contribution to debates on the international class structure and socialist strategy for the twenty-first century

- **Marx and Engels: On Colonies, Industrial Monopoly and the Working Class Movement** – A compilation of letters, excerpts and reflections from Marx and Engels on the impact of colonialism and nascent imperialism on the working 
  class movement, and the development of the embryonic labor aristocracy. 
  This work is highly accessible, with highlighted pull-quotes emphasizing
   important concepts in the collection. Also includes an extraordinary 
  introduction from Torkil Lauesen and Zak Cope putting the contents of 
  the work into a modern perspective.

- **Unequal Exchange** By Arghiri Emmanuel

- Manifest–Communist Working Group. [***Unequal Exchange and the Prospects of Socialism***](http://snylterstaten.dk/english/unequal-exchange-and-prospects-socialism-communist-working-group) – Unravels the mechanics of unequal exchange using Marxist political 
  economy, based mainly on the work of Arghiri Emmanuel. Very clear and 
  accessible. Also contains a solid overview / refresher on foundational 
  concepts such as value, surplus value, and price. **Beginner Friendly**

- **[MIM's Imperialism study pack](http://almhvxlkr4wwj7ah564vd4rwqk7bfcjiupyf7rs6ppcg5d7bgavbscad.onion/archive/books/Economics/ImperialismHighestStageofGlobalCapitalism.pdf)** collection of writings for understanding imperialism <mark>WARNING</mark> use tor

- **Che Guevara: The Economics of Revolution Yaffe, Helen.** – An important work from Helen Yaffe on the, as yet untranslated, works
   on political economy by Che Guevara. The work serves as an in-depth 
  criticism of not only capitalist restoration in the Soviet Union, but 
  the parasitic nature of the western workers and its impact on their 
  political impulses. Includes long translated quotations from Che’s 
  notes, as well as important summaries of his points on revisionism in 
  the Soviet Union, the New Economic Policy, the imperialist labor 
  aristocracy and the distortion of the world communist movement by 
  opportunists.

- **[Neo-Colonialism: The Last Stage of Imperialism](https://archive.org/details/unset0000unse_b2a2), Kwame Nkrumah**

- **[Imperialism in a Coffee Cup, John Smith](https://www.opendemocracy.net/en/oureconomy/imperialism-coffee-cup/)** Short article that illustrates imperialism in the 21st century

## Fascism

<mark>TBA </mark>

George Jackson

Georgy whatever his name is

MIM Study pack on Fascism

Ena͞emaehkiw Wākecānāpaew Kesīqnaeh. (2017). [“Fascism and Anti-Fascism: A Decolonial Perspective.”](https://www.indigenousaction.org/zine-fascism-anti-fascism-a-decolonial-perspective/)

## Eurocentrism and White Chauvinism

TBA 

- **[White BlindSpot](https://www.marxists.org/history/erol/ncm-1/whiteblindspot.pdf)**

- Satres Preface in Wretched of the Earth

- Lenins Great Russian Chauvinism essay 

- Biel Robert. [***Eurocentrism and the Communist Movement***](https://www.marxists.org/history/erol/uk.hightide/euro.pdf) – As the title implies, an extensive discussion of eurocentrism within Marxism. Explores eurocentric theory and practices within the broad communist movement of past and present, and also within the works of Marx and Engels themselves, in order to motivate a genuinely anti-imperialist Marxism.

- **How the Irish became white.** Ignatiev traces the tattered history of Irish and African-American relations, revealing how the Irish used labor unions, the Catholic Church and the Democratic party to help gain and secure their newly found place in the White Republic. He uncovers the roots of conflict between Irish-Americans & African-Americans & draws a powerful connection between the embracing of white supremacy & Irish "success" in 19th century American society.

- Bottomfish Blues. (2014). **Amazon Nation or Aryan Nation: White Women and the Coming of Black Genocide** These angry essays show how the massive New Afrikan uprisings of the 1960s were answered by the white ruling class: with the destruction of New Afrikan communities coast to coast, the decimation of the New Afrikan working class, the rise of the prison state and an explosion of violence between oppressed people. Taken on their own, in isolation, these blights may seem to be just more "social issues" for NGOs to get grants for, but taken together and in the context of amerikkkan history, they constitute genocide. **ask for copy**

- [**Zak Cope - Dimensions of Prejudice: Towards a Political Economy of Bigotry**](https://libgen.rs/book/index.php?md5=4484BB88B756A859274635FBCCF39F59)

## Revolutionary Nationalism

- **[Lenin: The Socialist Revolution and the Right of Nations to Self-Determination](https://www.marxists.org/archive/lenin/works/1916/jan/x01.htm)**

- **[Lenin: The Discussion On Self-Determination Summed Up](https://www.marxists.org/archive/lenin/works/1916/jul/x01.htm)**

- [**Lenin Draft Theses on National and Colonial Questions For The Second Congress Of The Communist International**](https://www.marxists.org/archive/lenin/works/1920/jun/05.htm)

- [**Minutes of Second Congress of the Communist International**](https://www.marxists.org/history/international/comintern/2nd-congress/ch04.htm)

- **[Huey Newton interview On Revolutionary Nationalism and Reactionary Nationalism](https://medium.com/@merricatherine/huey-p-newtons-interview-with-the-movement-magazine-1968-a328e6b78c32)** <mark>NOTE</mark> back this up asap

- **[MIM's Nationalism and Proletarian Feminism Study Pack](http://almhvxlkr4wwj7ah564vd4rwqk7bfcjiupyf7rs6ppcg5d7bgavbscad.onion/archive/books/mt/ProlFemRevNationalism_MIMPrisons.pdf)** <mark>WARNING</mark> USE TOR FOR LINK IT WON'T WORK WITHOUT.

- **[The National and Colonial Question  by Joseph Stalin](https://foreignlanguages.press/wp-content/uploads/2021/11/C28-Marxism-and-the-National-and-Colonial-Question-1st-Printing.pdf)**

- **[Message To The Grass Roots - Malcolm X - Invidious](https://yewtu.be/watch?v=a59Kwp35Z80)** **Video**

- Memmi, Albert. [***The Colonizer and the Colonized***](https://antiimperialism.files.wordpress.com/2017/01/the_colonizer_and_the_colonized_-_albert_memmi_jean-paul_sartre_nadine_gordimer_1991.pdf) – Unpacks the effects of colonialism on both the colonizer and the colonized. A good complementary text to Fanon’s *Wretched*. Contains great insight into the mindset of the colonizing people, 
  providing convincing explanations for reaction among poor colonizers.

- Sayles, James Yaki. [***Meditations on Frantz Fanon’s Wretched of the Earth***](https://antiimperialism.files.wordpress.com/2018/10/meditations.pdf) – A companion piece to Fanon’s *Wretched of the Earth* which not only helps one digest Fanon’s work, but also contains numerous great insights in its own right. Discusses how lessons from Wretched are still relevant today, even in the First World, and illuminates the lengths that are required in order for a people to liberate itself from oppression

- Shakur, Sanyika. [***Stand Up Struggle Forward: New Afrikan Revolutionary Writings On Nation, Class and Patriarchy***](https://mindlessb3havior.files.wordpress.com/2015/12/stand-up-struggle-forward_-new-shakur-sanyika-1.pdf) – A collection of writings which, among other things, make a compelling case for New Afrikan national liberation as opposed to what Shakur calls “radical integration.” Also contains a poignant perspective on the
  connection between gender oppression and national oppression. **MUST READ**

- **[Foundations of Leninism Chapter 6: the National Question ](https://www.marxists.org/reference/archive/stalin/works/1924/foundations-leninism/ch06.htm)**

- <mark>NOTE</mark> Add Ajiths neocolonial mind essay.

## Cuba

"A Nation for All” by Alejandro de la Fuente

## China

- **Science Walks on Two Legs** *description and link to be added later*

- **[The Chinese Road to Socialism: Economics of the Cultural Revolution by E.L. Wheelwright and Bruce MacFarlane](http://almhvxlkr4wwj7ah564vd4rwqk7bfcjiupyf7rs6ppcg5d7bgavbscad.onion/archive/books/China/ChineseRoadtoSocialism_WheelwrightMacFarlane.pdf)** **A MIM MUST READ**

- **From Victory to Defeat** How can a country that developed the most advanced socialist society in the history of the world change directions so quickly and so completely? In *From Victory to Defeat* Pao-yu Ching dissects this question, providing economic analysis of what it means to actually “build socialism” with all of the necessary contradictions and obstacles that must be overcome. **[link](https://foreignlanguages.press/new-roads/from-victory-to-defeat-pao-yu-ching/)**

- **bonus video video covering topics from the book** **[China's Socialist Development & Defeat](https://yewtu.be/watch?v=dfV3KhTjZzk&listen=1)**

- Park, Henry. [***Post-Revolutionary China and the Soviet NEP***](https://antiimperialism.files.wordpress.com/2018/10/henry_park_-_postrevolutionary_china_and_the_soviet_nep.pdf) – Analyzes capitalist restoration in China from the standpoint of the 
  Soviet New Economic Policy (NEP) and demonstrates the folly in believing
   such measures in China to be “temporary” or “harmless” as the country 
  allegedly continues its path toward communism.

###### Polemics of China Under Mao:

Write-ups re-evaluating the cultural revolution from beyond the incoherent praise of GPCR without understanding its dynamics that most parties seem to engage in. Ignore the leading light propaganda though:

1. **[Two Roads Defeated in the Cultural Revolution (Part 1 of 3): young generals and mass movements &#8211; Leading Light Communist Organization](https://llco.org/two-roads-defeated-in-the-cultural-revolution-part-1-of-3-young-generals-and-mass-movements/)**

2. **[Two Roads Defeated in the Cultural Revolution Part 2: Lin Biao’s Road &#8211; Leading Light Communist Organization](https://llco.org/two-roads-defeated-in-the-cultural-revolution-part-2-lin-biaos-road/)**

3. **[Two Roads Defeated Part 3: Proletarian Jacobins &#8211; Leading Light Communist Organization](https://llco.org/two-roads-defeated-part-3-proletarian-jacobins/)** 

4. **[Whither China? - Sheng-wu-lien | libcom.org](https://libcom.org/article/whither-china-sheng-wu-lien)**

## New Afrikan History/Theory

- **[Education to Govern](https://foreignlanguages.press/wp-content/uploads/2021/11/S23-Education-to-Govern-1st-Printing.pdf)**

- **[Why We say New Afrikan by MXGM]([Why We say &#8220;New Afrikan&#8221; &#8211; Malcolm X Grassroots Movement](https://freethelandmxgm.org/why-we-say-new-afrikan/))**

- **[The Roots of the New Afrikan Movement by Chokwe Lumumba](https://www.rebuildcollective.org/_files/ugd/633c20_2679ca1e84c9448cba8f31ac25f3977a.pdf)**

#### African Blood Brotherhood:

- **[History and Documents](https://www.marxists.org/history/usa/eam/other/abb/abb.html)**

#### Revolutionary Action Movement:

#### Black Panther Party:

- **[Ideology of the Black Panther Party](https://www.freedomarchives.org/Documents/Finder/Black%20Liberation%20Disk/Black%20Power%21/SugahData/Books/Cleaver.S.pdf)**

## Soviet Union

Park, Henry. [***Secondary Literature on the Question of the Restoration of Capitalism in the Soviet Union***](https://antiimperialism.files.wordpress.com/2018/10/henry-park-secondary-literature-on-the-restoration-of-capitalism-in-the-soviet-union-1.pdf) – An important summary and analysis of the restoration of capitalism in the USSR by former MIM Cadre, Henry Park. Includes references to many 
other first and secondary sources concerning capitalist restoration and 
the corrosion of socialism

## Chicano

## First Nations

- Smith, Andrea. ***Conquest: Sexual Violence and American Indian Genocide*** – Investigates the connection between sexual violence and the genocide of the indigenous people of “north america.” Uncovers how, for settlers, native bodies are viewed as “inherently rape-able,” and how rape and sexual dominance were / are internal to genocidal means employed against indigenous peoples, such as the u.s. boarding school system, environmental destruction, and gross medical mistreatment.

- Hill, Gord. ***500 Years of Indigenous Resistance*** – A concise introductory history of the conquest and genocide of indigenous peoples in occupied “north amerika”.

- Wolfe, Patrick. (2006). [“Settler Colonialism and the Elimination of the Native.”](http://gooriweb.org/genocide/89.pdf) *Journal of Genocide Research*.

- Ostler, Jeffrey. (2019). *Surviving Genocide: Native Nations and the United States from the American Revolution to Bleeding Kansas*.

- Lister, Majerle. (2018). [“The Only Way to Save the Land is to Give It Back’: A Critique of Settler Conservationism.”](https://therednation.org/the-only-way-to-save-the-land-is-to-give-it-back-a-critique-of-settler-conservationism/) *The Red Nation*.

## Socialism

**Rethinking Socialism**: This essay succinctly answers the question “Is China still socialist?” and tries to give an objective analysis of the reasons behind the defeat of socialism in China. This essay also helps with understanding what is and isn't socialism. [link]([Rethinking Socialism &#8211; Deng-yuan Hsu &#038; Pao-yu Ching &#8211; Foreign Languages Press](https://foreignlanguages.press/colorful-classics/rethinking-socialism-deng-yuan-hsu-pao-yu-ching/)

**[Socialism: Utopian and Scientific by Engels](https://www.marxists.org/archive/marx/works/1880/soc-utop/index.htm)**

## Haiti

Black Jacobins by CLR James

## Latin Amerika

Galeano, Eduardo. [***Open Veins of Latin America***](http://library.uniteddiversity.coop/More_Books_and_Reports/Open_Veins_of_Latin_America.pdf) – A landmark historical work on the political-economic history of colonialism and neo-colonialism in Latin America from a an unapologetically communist perspective. Galeano’s work here serves as an invaluable, albeit intensely depressing, resource on the conquest and domination of an entire continent.

## Organiztional Theory

- Freeman, Jo. [**The Tyranny of Structurelessness**](https://anti-imperialism.org/2017/05/02/the-tyranny-of-structurelessness/) – An important reflection and criticism of the most destructive tendencies of the late-20th century women’s movement and its implications in the broader revolutionary movement. Analyzes the ways in which opportunists and reactionaries utilize structurelessness in the 
  revolutionary movement to steer it toward liquidation for their personal gain. This document emphasizes the need for structure and centralism in the revolutionary movement, mostly through the negative examples of the past century. 

- [**Problems of Organizational Leadership**](https://www.anesi.com/east/stalin.htm)

#### Communist Party

- **[Lenin's What Is To Be Done?: Chapter 4: The Primitiveness of the Economists and the Organization of the Revolutionaries](https://www.marxists.org/archive/lenin/works/1901/witbd/iv.htm)**

- **[The Foundations of Leninism: Chapter VIII :THE PARTY](https://www.marxists.org/reference/archive/stalin/works/1924/foundations-leninism/ch08.htm)**

##### Forming a New Communist Party

1. **[V.I. Lenin, *What Is To Be Done?*, Chapter 3](https://www.marxists.org/archive/lenin/works/1901/witbd/iii.htm)**

2. [**V.I. Lenin, One Step Forward, Two Steps Back**](https://www.marxists.org/archive/lenin/works/1904/onestep/index.htm)

3. **History of the Communist Party of the Soviet Union (B)**, Chapters 1-2
- [**Chapter 1: THE STRUGGLE FOR THE CREATION OF A SOCIAL-DEMOCRATIC LABOUR PARTY IN RUSSIA  (1883 - 1901)**](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm) 
  
         
  
      [**1**](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm#1.). Abolition of Serfdom and the Development of Industrial Capitalism in 
  Russia. Rise of the Modern Industrial Proletariat. First Steps of the 
  Working-Class Movement
  
            

          [2](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm#2.). Narodism (Populism) and Marxism in Russia. Plekhanov and His 
 “Emancipation of Labour” Group. Plekhanov’s Fight Against Narodism. 
 Spread of Marxism in Russia

          [3](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm#3.). Beginning of Lenin’s Revolutionary Activities. St. Petersburg League of Struggle for the Emancipation of the Working Class

          [4](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm#4.). Lenin’s Struggle Against Narodism and “Legal Marxism.” Lenin’s Idea of
  an Alliance of the Working Class and the Peasantry. First Congress of 
 the Russian Social-Democratic Labour Party

         [5](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch01.htm#5._). Lenin’s Fight Against “Economism.” Appearance of Lenin’s Newspaper *Iskra*

- [**Chapter 2: FORMATION OF THE RUSSIAN SOCIAL-DEMOCRATIC LABOUR PARTY. APPEARANCE OF THE BOLSHEVIK AND THE MENSHEVIK GROUPS WITHIN THE PARTY (1901 - 1904 )**](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch02.htm)
  
  ****
  
      [1](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch02.htm#1.). Upsurge of the Revolutionary Movement in Russia in 1901-04
  
      
      [2](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch02.htm#2.). Lenin’s Plan for the Building of a Marxist Party. Opportunism of the 
  “Economists.” Iskra’s Fight for Lenin’s Plan. Lenin’s Book *What Is To Be Done?* Ideological Foundations of the Marxist Party
  
      
  
      [3](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch02.htm#3._). Second Congress of the Russian Social-Democratic Labour Party. Adoption of Program and Rules and Formation of a Single Party. 
  Differences at the Congress and Appearance of Two Trends Within the 
  Party: The Bolshevik and the Menshevik

          [4](https://www.marxists.org/reference/archive/stalin/works/1939/x01/ch02.htm#4.). Splitting Activities of the Menshevik Leaders and Sharpening of the 
 Struggle Within the Party After the Second Congress. Opportunism of the 
 Menshevik. Lenin’s Book, *One Step Forward, Two Steps Back*. Organizational Principles of the Marxist Party

 ****

  What tasks did Lenin see as necessary before the actual Party Congress could take place?

## 

## Proletariat Feminism

- [**The Womens Movement In China**](https://www.bannedthought.net/China/MaoEra/ContemporaryCommentary/Anglo-ChineseEdInst/Pubs/WomensMovementInChina-Croll-MC-6-1974.pdf)

## 

## Additional Resources

TBA:

Malcolm X speechs

Hauani Kay Trask vids and material

Media Section

India Section

Assata Shakur

Caste
